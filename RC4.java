import java.util.*;

class RC4 {
	int keylen;
	int N;
	int size;
	int[] S;
	int[] K;
	int[] P, G, C;
    String Gs;
    String Cs;
	
	public RC4(String key, String plaintext) {
		keylen = key.length();
		K = new int[keylen];
		for(int i = 0; i < keylen; i++) {
			K[i] = key.charAt(i);
		}
		
		N = plaintext.length();
		P = new int[N];
		G = new int[N];
		C = new int[N];
		
		for(int i = 0; i < N; i++) {
			P[i] = plaintext.charAt(i);
		}
		size = 256;
		S = new int[size];
		Gs = "";
		Cs = "";
	}
	
	public static void main(String[] args) {
		RC4 rc4 = new RC4("Secret", "Mohit Kanodia");
		rc4.initialization();
		rc4.streamGeneration();
		rc4.encrypt();
        rc4.convToHex();
        rc4.display();
	}
	
	public void initialization() {
		for(int i = 0; i < size; i++) {
			S[i] = i;
		}
		int j = 0, k = 0;
		for(int i = 0; i < size; i++) {
			k = K[i % keylen];
			j = (j+S[i]+k) % size;
			swap(S, i, j);
		}
	}
	
	public void streamGeneration() {
		int i = 0;
		int j = 0;
		int t = 0;
		for(int k = 0; k < N; k++) {
			i = (i + 1) % size;
			j = (j + S[i]) % size;
			swap(S, i, j);
			t = (S[i] + S[j]) % size;
			G[k] = S[t];
		}
	}
	
	public void encrypt() {
		for(int i = 0; i < N; i++) {
			C[i] = P[i]^G[i];
		}
	}
	
	public void decrypt() {
		for(int i = 0; i < N; i++) {
			P[i] = C[i]^G[i];
		}
	}
	
	private void swap(int[] S, int i, int j) {
		int temp = S[i];
		S[i] = S[j];
		S[j] = temp;
	}
	
	public void display() {
		System.out.println("PlainText:");
		System.out.println(Arrays.toString(P));
		System.out.println("Key:");
		System.out.println(Arrays.toString(K));
		System.out.println("PRNG Stream:");
		System.out.println(Arrays.toString(G));
        System.out.println(Gs);
		System.out.println("CipherText:");
		System.out.println(Arrays.toString(C));
        System.out.println(Cs);
	}

    public void convToHex() {
        Gs = convToHex(G);
        Cs = convToHex(C);
    }

    private String convToHex(int[] G) {
        String ans = "";
        for(int i = 0; i < G.length; i++) {
            int n = G[i];
            int r  = 0;
            String res = "";
            while(n > 0) {
                r = n % 16;
                if(r < 10)
                    res = (char)('0'+r) + res;
                else
                    res = (char)('A'+(r-10)) + res;
                n = n/16;
            }
            if(res.length() == 1)
                res = '0'+res;
            ans += res;
        }
        return ans;
    }

}

